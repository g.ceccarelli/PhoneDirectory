package it.anoki.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.anoki.dao.AziendaDAO;
import it.anoki.model.Azienda;
import it.anoki.model.RecapitoAzienda;
import it.anoki.model.RecapitoPersona;
import it.anoki.service.AziendaService;

@RestController
@RequestMapping("/azienda")
@CrossOrigin(origins = "*")
public class AziendaController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Autowired
	AziendaService aziendaService;
	
	@GetMapping("/")
	public List<Azienda> getAziende(){
		return aziendaDAO.findAll();
	}

	@GetMapping("/id/{aziendaId}")
	public Azienda getAzienda(@PathVariable("aziendaId") long id){
		return aziendaDAO.findById(id);
	}
	
	@GetMapping("/ragionesociale/{ragioneSociale}")
	public Azienda getAzienda(@PathVariable("ragioneSociale") String azienda){
		Azienda azienda2 = aziendaDAO.findByRagioneSociale(azienda);
		System.err.println(azienda2.getPersone());
		return aziendaDAO.findByRagioneSociale(azienda);
	}
	
	@DeleteMapping("/{id}")
	public void cancellaAzienda(@PathVariable("id") long id){
		aziendaDAO.delete(id);
	}
	
	@PostMapping("/")
	public Azienda aggiungiAzienda(@RequestBody Azienda azienda){
		try{
			return aziendaDAO.createAzienda(azienda);
		}catch(DataIntegrityViolationException e){
			logger.error("RagioneSociale is already inserted!");
			return null;
		}
	}
	
	@PutMapping("/{id}")
	public void aggiornaAzienda(@PathVariable("id") long id,
			@RequestBody Azienda azienda){
		if(id != azienda.getId())
			throw new IllegalArgumentException("Cannot update a different company!");
		aziendaDAO.updateAzienda(id, azienda);
	}
	
	@RequestMapping("/restartID")
	public void restart(){
		aziendaDAO.restart();
	}
	
	
	@GetMapping("/contatti/{ragioneSociale}")
	public List<RecapitoAzienda> getRecapitiAzienda(@PathVariable("ragioneSociale") String ragioneSociale){
		return aziendaService.getRecapitiAzienda(ragioneSociale);
	}
	
	 @GetMapping("/dipendenti/{ragioneSociale}")
	 public List<RecapitoPersona> getDipendenti(@PathVariable("ragioneSociale") String ragioneSociale){
		 return aziendaService.getRecapitiDipendenti(ragioneSociale);
	 }
	
	

}
