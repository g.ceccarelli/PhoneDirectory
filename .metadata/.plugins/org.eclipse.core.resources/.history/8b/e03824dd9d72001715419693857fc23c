package it.anoki.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.anoki.dao.PersonaDAO;
import it.anoki.model.Persona;

@RestController
@RequestMapping("/persona")
@CrossOrigin(origins = "*")
public class PersonaController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	PersonaDAO personaDAO;
	
	
	@PostMapping("/")
	public Persona aggiungiPersona(@RequestBody Persona persona){
		try{
			return personaDAO.createPersona(persona);
		}catch(DataIntegrityViolationException e){
			logger.error("Attention: unique constraint!");
			return null;
		}
	}
	
	@GetMapping("/")
	public List<Persona> getPersone(){
		return personaDAO.findAll();
	}
	
	@GetMapping("/id/{id}")
	public Persona getPersona(@PathVariable("id") long id){
		return personaDAO.findByID(id);
	}
	
	@GetMapping("/email/{email}")
	public Persona getPersona(@PathVariable("email") String email){
		return personaDAO.findByEmail(email);
	}
	
	@DeleteMapping("/{id}")
	public void cancellaPersona(@PathVariable("id") long id){
		personaDAO.deletePersona(id);
	}
	
	@PutMapping("/{id}")
	public Persona aggiornaPersona(@PathVariable("id") long id,
			@RequestBody Persona persona){
		if(id!= persona.getId())
			throw new IllegalArgumentException("Cannot update a different Person");
		return personaDAO.updatePersona(persona);
	}
	
	@RequestMapping("/restartID")
	public void restart(){
		personaDAO.restart();
	}
	
	

}
