package it.anoki.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Persona;

@Repository
public class PersonaDAO {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext
	EntityManager entityManager;
	
	/*------------------- CREATE --------------------*/
	@Transactional(readOnly = false)
	public void createPersona(Persona persona){
		entityManager.persist(persona);
	}
	
	/*------------------- READ --------------------*/
	@Transactional(readOnly=true)
	public List<Persona> findAll(){
		return entityManager.createQuery("FROM Persona", Persona.class).getResultList();
	}
	
	@Transactional(readOnly = true)
	public Persona findByID(long id){
		Persona persona = entityManager.find(Persona.class, id);
		if(persona==null)
			logger.warn("Id non presente");
		return persona;
	}
	
	@Transactional(readOnly=true)
	public Persona findByEmail(String email){
		String hql="from Persona p where p.email = :email";
		Query query = entityManager.createQuery(hql);
		query.setParameter("email", email);
		try{
			return (Persona) query.getSingleResult();
		}catch(NoResultException e){
			logger.error("Email non presente");
			return null;
		}
	}
	
	/*------------------- UPDATE --------------------*/
	@Transactional(readOnly=false)
	public void updatePersona(Persona persona){
		entityManager.merge(persona);
	}
	
	/*------------------- DELETE --------------------*/
	@Transactional(readOnly=false)
	public void deletePersona(long id){
		Persona persona = this.findByID(id);
		System.err.println(persona);
		if(persona.getUtente()==null)
			entityManager.remove(persona);
		else
			logger.warn("Non è possibile rimuovere persona:foreign key");
	}
	
	
	/*------------------- DA SPOSTARE NEL SERVICE --------------------*/

	@Transactional(readOnly = false)
	public void restart(){
		String sql = "ALTER SEQUENCE persona_persona_id_seq RESTART WITH 1";
		entityManager.createNativeQuery(sql).executeUpdate();
	}

}

