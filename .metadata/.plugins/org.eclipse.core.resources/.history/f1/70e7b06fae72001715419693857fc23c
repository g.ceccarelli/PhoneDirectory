package it.anoki.controller;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.anoki.dao.AziendaDAO;
import it.anoki.model.Azienda;
import it.anoki.model.RecapitoAzienda;
import it.anoki.model.RecapitoPersona;
import it.anoki.service.AziendaService;

@RestController
@RequestMapping("/azienda")
@CrossOrigin(origins = "*")
public class AziendaController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Autowired
	AziendaService aziendaService;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@GetMapping("/")
	public List<Azienda> getAziende(){
		return aziendaDAO.findAll();
	}

	@GetMapping("/id/{aziendaId}")
	public Azienda getAzienda(@PathVariable("aziendaId") long id){
		return aziendaDAO.findById(id);
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/ragionesociale/{ragioneSociale}")
	public <T> ResponseEntity<T> getAzienda(@PathVariable("ragioneSociale") String azienda) throws JSONException{
		Azienda ris = aziendaDAO.findByRagioneSociale(azienda);
		if(ris==null){
			ObjectNode jsn = objectMapper.createObjectNode();
			jsn.put("error", "ragionesociale");
	        return (ResponseEntity<T>) ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(jsn);
		}else{
			return (ResponseEntity<T>) ResponseEntity.ok(ris);
		}
	}
	
	@DeleteMapping("/{id}")
	public void cancellaAzienda(@PathVariable("id") long id){
		aziendaDAO.delete(id);
	}
	
	@PostMapping("/")
	public Azienda aggiungiAzienda(@RequestBody Azienda azienda){
		try{
			return aziendaDAO.createAzienda(azienda);
		}catch(DataIntegrityViolationException e){
			logger.error("RagioneSociale is already inserted!");
			return null;
		}
	}
	
	@PutMapping("/{id}")
	public void aggiornaAzienda(@PathVariable("id") long id,
			@RequestBody Azienda azienda){
		if(id != azienda.getId())
			throw new IllegalArgumentException("Cannot update a different company!");
		aziendaDAO.updateAzienda(id, azienda);
	}
	
	@RequestMapping("/restartID")
	public void restart(){
		aziendaDAO.restart();
	}
	
	
	@GetMapping("/contatti/{ragioneSociale}")
	public List<RecapitoAzienda> getRecapitiAzienda(@PathVariable("ragioneSociale") String ragioneSociale){
		return aziendaService.getRecapitiAzienda(ragioneSociale);
	}
	
	 @GetMapping("/dipendenti/{ragioneSociale}")
	 public List<RecapitoPersona> getDipendenti(@PathVariable("ragioneSociale") String ragioneSociale){
		 return aziendaService.getRecapitiDipendenti(ragioneSociale);
	 }
	
	

}
