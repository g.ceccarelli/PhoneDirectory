package DTO;

public class UtenteDTO {
	
	String mail;
	String password;
	
	public UtenteDTO(){}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	};
	
	

}
