package it.anoki.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.anoki.dao.PersonaDAO;
import it.anoki.model.Persona;
import it.anoki.service.PersonaService;

@RestController
@RequestMapping("/persona")
@CrossOrigin(origins = "*")
public class PersonaController {
		
	@Autowired
	PersonaDAO personaDAO;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private PersonaService personaService;
	
	
	@PostMapping("/")
	public ResponseEntity aggiungiPersona(@RequestBody Persona persona){
		try{
			return ResponseEntity.ok(personaDAO.createPersona(persona));
		}catch(DataIntegrityViolationException e){
			ObjectNode jsn = objectMapper.createObjectNode();
			jsn.put("error", "anagrafici");
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsn);
		}
	}
	
	@GetMapping("/")
	public List<Persona> getPersone(){
		return personaDAO.findAll();
	}
	
	@GetMapping("/id/{id}")
	public Persona getPersona(@PathVariable("id") long id){
		return personaDAO.findByID(id);
	}
	
	@GetMapping("/email/{email}")
	public ResponseEntity getPersona(@PathVariable("email") String email) throws UnsupportedEncodingException{
		String result = URLDecoder.decode(email, "UTF-8");
		Persona persona = personaDAO.findByEmail(result);
		if(persona==null){
			ObjectNode jsn = objectMapper.createObjectNode();
			jsn.put("error", "email");
	        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(jsn);
		}else{
			return ResponseEntity.ok(persona);
		}
	}
	
	@GetMapping("duplicatename/{nome}/{cognome}")
	public ResponseEntity getDuplicateName(@PathVariable("nome") String nome,@PathVariable("cognome") String cognome ){
		ObjectNode jsn = objectMapper.createObjectNode();
		jsn.put("duplicato", personaService.findDuplicateName(nome, cognome));
		return ResponseEntity.ok(jsn);
	}
	
	@GetMapping("duplicateemail/{email}")
	public ResponseEntity getDuplicateEmail(@PathVariable("email") String email) throws UnsupportedEncodingException{
		String result = URLDecoder.decode(email, "UTF-8");
		ObjectNode jsn = objectMapper.createObjectNode();
		jsn.put("duplicato", personaService.findDuplicateEmail(result));
		return ResponseEntity.ok(jsn);
	}
	
	@DeleteMapping("/{id}")
	public void cancellaPersona(@PathVariable("id") long id){
		personaDAO.deletePersona(id);
	}
	
	@PutMapping("/{id}")
	public Persona aggiornaPersona(@PathVariable("id") long id,
			@RequestBody Persona persona){
		if(id!= persona.getId())
			throw new IllegalArgumentException("Cannot update a different Person");
		return personaDAO.updatePersona(persona);
	}
	
	@RequestMapping("/restartID")
	public void restart(){
		personaDAO.restart();
	}
	
	

}
