package it.anoki.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.anoki.dao.RecapitoAziendaDAO;
import it.anoki.model.RecapitoAzienda;
import it.anoki.service.RecapitoAziendaService;

@RestController
@RequestMapping("/recapitoAzienda")
@CrossOrigin(origins = "*")
public class RecapitoAziendaController {

	@Autowired
	RecapitoAziendaDAO recapitoAziendaDAO;
	
	@Autowired
	RecapitoAziendaService recapitoAziendaService;
		
	@PostMapping("/")
	public void aggiungiRecapitoAzienda(@RequestBody RecapitoAzienda recapitoAzienda){
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
	}
	
	@GetMapping("/")
	public List<RecapitoAzienda> getAziende(){
		return recapitoAziendaDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public RecapitoAzienda getRecapitoAziendale(@PathVariable("id") long id){
		return recapitoAziendaDAO.findById(id);
	}
	
	@DeleteMapping("/{id}")
	public void cancellaRecapitoAzienda(@PathVariable("id") long id){
		recapitoAziendaDAO.deleteRecapitoAzienda(id);
	}
	
	@PutMapping("/{id}")
	public void aggiornaRecapitoAzienda(@PathVariable("id") long id,
			@RequestBody RecapitoAzienda recapitoAzienda){
		if(id!= recapitoAzienda.getId())
			throw new IllegalArgumentException("Cannot update a different Company Phone!");
		recapitoAziendaDAO.updateRecapitoAzienda(recapitoAzienda);
	}
	
	@PutMapping("/up/{id}")
	public void up(@PathVariable("id") long id, 
			@RequestBody RecapitoAzienda recapitoAzienda){
		if(id!= recapitoAzienda.getId())
			throw new IllegalArgumentException("Cannot update a different Company Phone!");
		recapitoAziendaService.up(recapitoAzienda);
	}
	
	@PutMapping("/down/{id}")
	public void down(@PathVariable("id") long id, 
			@RequestBody RecapitoAzienda recapitoAzienda){
		if(id!= recapitoAzienda.getId())
			throw new IllegalArgumentException("Cannot update a different Company Phone!");
		recapitoAziendaService.down(recapitoAzienda);

	}
	
	
}
