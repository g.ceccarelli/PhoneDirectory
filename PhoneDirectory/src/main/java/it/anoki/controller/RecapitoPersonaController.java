package it.anoki.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.anoki.dao.RecapitoPersonaDAO;
import it.anoki.model.RecapitoPersona;
import it.anoki.service.RecapitoPersonaService;

@RestController
@RequestMapping("/recapitoPersona")
@CrossOrigin(origins = "*")
public class RecapitoPersonaController {

	@Autowired
	RecapitoPersonaDAO recapitoPersonaDAO;
	
	@Autowired
	RecapitoPersonaService recapitoPersonaService;
	
	@PostMapping("/")
	public void aggiungiRecapitoPersona(@RequestBody RecapitoPersona recapitoPersona){
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona);
	}
	
	@GetMapping("/")
	public List<RecapitoPersona> getRecapitoAziendale(){
		return recapitoPersonaDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public RecapitoPersona getRecapitoPersona(@PathVariable("id") long id){
		return recapitoPersonaDAO.findById(id);
	}
	
	@DeleteMapping("/{id}")
	public void cancellaRecapitoPersona(@PathVariable("id") long id){
		recapitoPersonaDAO.deleteRecapitoPersona(id);
	}
	
	@PutMapping("/{id}")
	public void aggiornaRecapitoPersona(@PathVariable("id") long id,
			@RequestBody RecapitoPersona recapitoPersona){
		if(id!=recapitoPersona.getId())
			throw new IllegalArgumentException("Cannot updare a different Personal Phone ");
		recapitoPersonaDAO.updateRecapitoPersona(recapitoPersona);
	}
	
	@PutMapping("/up/{id}")
	public void up(@PathVariable("id") long id,
			@RequestBody RecapitoPersona recapitoPersona){
		if(id!= recapitoPersona.getId())
			throw new IllegalArgumentException("Cannot updare a different Personal Phone ");
		recapitoPersonaService.up(recapitoPersona);
	}
	
	@PutMapping("/down/{id}")
	public void down(@PathVariable("id") long id,
			@RequestBody RecapitoPersona recapitoPersona){
		if(id!= recapitoPersona.getId())
			throw new IllegalArgumentException("Cannot updare a different Personal Phone ");
		recapitoPersonaService.down(recapitoPersona);
	}
}
