package it.anoki.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.anoki.dao.TelefonoDAO;
import it.anoki.model.Telefono;
import it.anoki.service.RecapitoPersonaService;

@RestController
@RequestMapping("/telefono")
@CrossOrigin(origins = "*")
public class TelefonoController {
	
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	TelefonoDAO telefonoDAO;
	
	@Autowired
	RecapitoPersonaService telefonoService;
	
	@PostMapping("/")
	public void aggiungiTelefono(@RequestBody Telefono telefono){
		try{
			telefonoDAO.createTelefono(telefono);
		}catch(DataIntegrityViolationException e){
			logger.error("Numero già presente!");
		}
	}
	
	@GetMapping("/")
	public List<Telefono> getTelefono(){
		return telefonoDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public Telefono getTelefono(@PathVariable("id") long id){
		return telefonoDAO.findByID(id);
	}
	
	@DeleteMapping("/{id}")
	public void cancellaTelefono(@PathVariable("id") long id){
		telefonoDAO.deleteTelefono(id);
	}
	
	@PutMapping("/{id}")
	public void aggiornaTelefono(@PathVariable("id") long id,
			@RequestBody Telefono telefono){
		if(id!=telefono.getId())
			throw new IllegalArgumentException("Cannot update a different Phone");
		telefonoDAO.updateTelefono(telefono);
	}
	
	@GetMapping("/restartID")
	public void restart(){
		telefonoDAO.restart();
	}
	
	
	
	

}
