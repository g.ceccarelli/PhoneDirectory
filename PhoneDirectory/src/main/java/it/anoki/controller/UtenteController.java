package it.anoki.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import DTO.UtenteDTO;
import it.anoki.dao.UtenteDAO;
import it.anoki.model.Utente;
import it.anoki.service.UtenteService;

@RestController
@RequestMapping("/utente")
@CrossOrigin(origins = "*")
public class UtenteController {
	
	@Autowired
	UtenteDAO utenteDAO;

	@Autowired
	UtenteService utenteService;
	
	@PostMapping("/")
	public void aggiungiUtente(@RequestBody Utente utente){
		utenteDAO.createUtente(utente);
	}
	
	@GetMapping("/")
	public List<Utente> getUtenti(){
		return utenteDAO.findAll();
	}
	
	@GetMapping("/{id}")
	public Utente getUtente(@PathVariable("id") long id){
		return utenteDAO.findByID(id);
	}
	
	@DeleteMapping("/{id}")
	public void cancellaUtente(@PathVariable("id") long id){
		utenteDAO.deleteUtente(id);
	}
	
	@PutMapping("/")
	public void aggionaUtente(@PathVariable("id") long id,
			@RequestBody Utente utente){
		if(id != utente.getId())
			throw new IllegalArgumentException("Cannot update a different user");
		utenteDAO.updateUtente(utente);
	}
	
	@RequestMapping("/restartID")
	public void restart(){
		utenteDAO.restart();
	}
	
	//
	@PostMapping("/login/")
	public void getPersona(@RequestBody UtenteDTO utente){
		utenteService.login(utente);
	}

}
