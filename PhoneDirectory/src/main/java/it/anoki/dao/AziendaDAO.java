package it.anoki.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Azienda;

@Repository
public class AziendaDAO {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext
	EntityManager entityManager;
	
	/*------------------- CREATE --------------------*/
	@Transactional(readOnly = false)
	public Azienda createAzienda(Azienda azienda){
		if(azienda.getRagioneSociale()==null){
			logger.error("ragionesociale null");
			azienda = null;
		}else
			entityManager.persist(azienda);
		return azienda;
	}
	
	/*------------------- READ --------------------*/
	@Transactional(readOnly = true)
	public List<Azienda> findAll(){
		return entityManager.createQuery("from Azienda", Azienda.class).getResultList();
	}
	
	@Transactional(readOnly = true)
	public Azienda findById(long id){
		Azienda azienda = entityManager.find(Azienda.class, id);
		if(azienda==null)
			logger.warn("Id not exist");
		return azienda;
	}
	
	@Transactional(readOnly = true)
	public Azienda findByRagioneSociale(String nomeAzienda){
		String hql = "from Azienda A where A.ragioneSociale = :ragioneSociale";
		Query query = entityManager.createQuery(hql);
		query.setParameter("ragioneSociale", nomeAzienda);
		try{
			return (Azienda) query.getSingleResult();
		}catch(NoResultException e){
			logger.error("RagioneSociale non presente!");
			return null;
		}
	}
	
	/*------------------- UPDATE --------------------*/
	@Transactional(readOnly = false)
	public Azienda updateAzienda(long id, Azienda azienda){
		return entityManager.merge(azienda);
	}
	
	/*------------------- DELETE --------------------*/
	@Transactional(readOnly = false)
	public void delete(long id){
		Azienda azienda = this.findById(id);
		if(azienda!=null && azienda.getPersone().isEmpty() && azienda.getContatti().isEmpty()){
			entityManager.remove(azienda);
		}else{
			logger.error("Non è possibile rimuovere azienda: foreign key");
		}
	}
	/*------------------- UTILITIES --------------------*/

	@Transactional(readOnly = false)
	public void restart(){
		String sql = "ALTER SEQUENCE azienda_azienda_id_seq RESTART WITH 1";
		entityManager.createNativeQuery(sql).executeUpdate();
	}
	
	@Transactional
	public void clear(){
		entityManager.clear();
	}
}
