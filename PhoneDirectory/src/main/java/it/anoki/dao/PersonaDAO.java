

package it.anoki.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Persona;

@Repository
public class PersonaDAO {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext
	EntityManager entityManager;
	
	/*------------------- CREATE --------------------*/
	@Transactional(readOnly = false)
	public Persona createPersona(Persona persona){
		if(persona.getEmail()==null || persona.getAzienda()==null){
			logger.error("email or azienda null!");
			persona = null;
		}else
			entityManager.persist(persona);
		return persona;
	}
	
	/*------------------- READ --------------------*/
	@Transactional(readOnly=true)
	public List<Persona> findAll(){
		return entityManager.createQuery("FROM Persona", Persona.class).getResultList();
	}
	
	@Transactional(readOnly = true)
	public Persona findByID(long id){
		Persona persona = entityManager.find(Persona.class, id);
		if(persona==null)
			logger.warn("Id non presente");
		return persona;
	}
	
	@Transactional(readOnly=true)
	public Persona findByEmail(String email){
		Persona persona = null;
		String hql="from Persona p where p.email = :email";
		Query query = entityManager.createQuery(hql);
		query.setParameter("email", email);
		try{
			persona = (Persona) query.getSingleResult();
		}catch(NoResultException e){
			logger.error("Email non presente");
			persona = null;
		}
		return persona;
	}
	
	/*------------------- UPDATE --------------------*/
	@Transactional(readOnly=false)
	public Persona updatePersona(Persona persona){
		return entityManager.merge(persona);
	}
	
	/*------------------- DELETE --------------------*/
	@Transactional(readOnly=false)
	public void deletePersona(long id){
		Persona persona = this.findByID(id);
		if(persona.getUtente()==null)
			entityManager.remove(persona);
		else
			logger.warn("Non è possibile rimuovere persona:foreign key");
	}
	
	/*------------------- UTILITIES --------------------*/

	@Transactional(readOnly = false)
	public void restart(){
		String sql = "ALTER SEQUENCE persona_persona_id_seq RESTART WITH 1";
		entityManager.createNativeQuery(sql).executeUpdate();
	}
	
	@Transactional
	public void clear(){
		entityManager.clear();
	}
	

}

