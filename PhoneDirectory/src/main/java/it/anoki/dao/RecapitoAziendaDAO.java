package it.anoki.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.RecapitoAzienda;

@Repository
public class RecapitoAziendaDAO {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@PersistenceContext
	EntityManager entityManager;
	
	/*------------------- CREATE --------------------*/
	@Transactional(readOnly=false)
	public RecapitoAzienda createRecapitoAzienda(RecapitoAzienda recapitoAzienda){
		if(recapitoAzienda.getDescrizione()==null || recapitoAzienda.getAzienda()==null
				|| recapitoAzienda.getTelefonoAzienda()==null){
			logger.warn("Cannot insert RecapitoAzienda");
			recapitoAzienda=null;
		}else
			entityManager.persist(recapitoAzienda);
		return recapitoAzienda;
	}
	
	/*------------------- READ --------------------*/
	@Transactional(readOnly=true)
	public List<RecapitoAzienda> findAll(){
		return entityManager.createQuery("FROM RecapitoAzienda",
				RecapitoAzienda.class).getResultList();
	}
	
	@Transactional(readOnly=false)
	public RecapitoAzienda findById(long id){
		RecapitoAzienda recapito = entityManager.find(RecapitoAzienda.class, id);
		if(recapito==null)
			logger.warn("Id not exist");
		return recapito;
	}
	
	/*------------------- UPDATE --------------------*/
	@Transactional(readOnly=false)
	public void updateRecapitoAzienda(RecapitoAzienda recapitoAzienda){
		entityManager.merge(recapitoAzienda);
	}
	
	/*------------------- DELETE --------------------*/
	@Transactional(readOnly=false)
	public void deleteRecapitoAzienda(long id){
		RecapitoAzienda recapitoAzienda = this.findById(id);
		if(recapitoAzienda!=null)
			entityManager.remove(recapitoAzienda);
		else
			logger.warn("It is impossible delete Company Phone: foreing key");
	}
	
	/*------------------- UTILITIES --------------------*/

	@Transactional(readOnly = false)
	public void restart(){
		String sql = "ALTER SEQUENCE recapito_azienda_recapito_azienda_id_seq RESTART WITH 1";
		entityManager.createNativeQuery(sql).executeUpdate();
	}
	
	@Transactional
	public void clear(){
		entityManager.clear();
	}
	
	


}
