package it.anoki.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.RecapitoPersona;

@Repository
public class RecapitoPersonaDAO {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext
	EntityManager entityManager;
	
	/*------------------- CREATE --------------------*/
	@Transactional(readOnly=false)
	public RecapitoPersona createRecapitoPersona(RecapitoPersona recapitoPersona){
		if(recapitoPersona.getDescrizione()==null || recapitoPersona.getTelefonoPersona()==null
				|| recapitoPersona.getPersona()==null){
			logger.warn("Cannot insert RecapitoPersona");
			recapitoPersona=null;
		}else
			entityManager.persist(recapitoPersona);
		return recapitoPersona;
	}
	
	/*------------------- READ --------------------*/
	@Transactional(readOnly=true)
	public List<RecapitoPersona> findAll(){
		return entityManager.createQuery("FROM RecapitoPersona", RecapitoPersona.class).getResultList();
	}
	
	@Transactional(readOnly=true)
	public RecapitoPersona findById(long id){
		RecapitoPersona recapito = entityManager.find(RecapitoPersona.class, id);
		if(recapito==null)
			logger.warn("Id not exist");
		return recapito;
	}
	/*------------------- UPDATE --------------------*/
	@Transactional(readOnly=false)
	public RecapitoPersona updateRecapitoPersona(RecapitoPersona recapitoPersona){
		return entityManager.merge(recapitoPersona);
	}
	
	/*------------------- DELETE --------------------*/
	@Transactional(readOnly=false)
	public void deleteRecapitoPersona(long id){
		RecapitoPersona recapitoPersona = this.findById(id);
		if(recapitoPersona!=null)
			entityManager.remove(recapitoPersona);
		else
			logger.warn("It is impossible delete Personal Phone: foreign key");
	}
	
	/*------------------- UTILITIES --------------------*/

	@Transactional(readOnly = false)
	public void restart(){
		String sql = "ALTER SEQUENCE recapito_persona_recapito_persona_id_seq RESTART WITH 1";
		entityManager.createNativeQuery(sql).executeUpdate();
	}
	
	@Transactional
	public void clear(){
		entityManager.clear();
	}

	
}
