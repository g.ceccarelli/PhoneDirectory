package it.anoki.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Telefono;

@Repository
public class TelefonoDAO {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext
	EntityManager entityManager;
	
	/*------------------- CREATE --------------------*/
	@Transactional(readOnly=false)
	public Telefono createTelefono(Telefono telefono){
		Telefono phoneNumber;
		if(telefono.getNumero()==null){
			logger.warn("Cannot create phone Number! Field number is null!");
			phoneNumber=null;
		}else{
			entityManager.persist(telefono);
			phoneNumber= telefono;
		}
		return phoneNumber;
	}
	/*------------------- READ --------------------*/
	@Transactional(readOnly=true)
	public List<Telefono> findAll(){
		return entityManager.createQuery("FROM Telefono", Telefono.class).getResultList();
	}
	
	@Transactional(readOnly=true)
	public Telefono findByID(long id){
		Telefono telefono = entityManager.find(Telefono.class, id);
		if(telefono==null)
			logger.warn("Id non presente");
		return telefono;
	}
	
	@Transactional(readOnly=true)
	public Telefono findByNumero(String numero){
		String hql="from Telefono p where p.numero = :numero";
		Query query = entityManager.createQuery(hql);
		query.setParameter("numero", numero);
		try{
			return (Telefono) query.getSingleResult();
		}catch(NoResultException e){
			logger.error("numero non presente");
			return null;
		}
	}
	/*------------------- DELETE --------------------*/
	@Transactional(readOnly=false)
	public void deleteTelefono(long id){
		Telefono telefono = this.findByID(id);
		if(telefono!= null 
				&& telefono.getRecapitoPersona().isEmpty()
				&& telefono.getRecapitoAzienda()== null){
			entityManager.remove(telefono);
		}else{
			logger.warn("Non è possibile rimuovere telefono: foreign key");
		}
	}
	/*------------------- UPDATE --------------------*/
	@Transactional(readOnly=false)
	public Telefono updateTelefono(Telefono telefono){
		return entityManager.merge(telefono);
	}
	
	/*------------------- UTILITIES --------------------*/
	@Transactional(readOnly=false)
	public void restart() {
		String sql = "ALTER SEQUENCE telefono_telefono_id_seq RESTART WITH 1";
		entityManager.createNativeQuery(sql).executeUpdate();
	}
	
	@Transactional
	public void clear(){
		entityManager.clear();
	}
	
}
