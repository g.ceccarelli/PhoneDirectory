package it.anoki.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Utente;

@Repository
public class UtenteDAO {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PersistenceContext
	EntityManager entityManager;
	
	/*------------------- CREATE --------------------*/
	@Transactional(readOnly=false)
	public void createUtente(Utente utente){
		entityManager.persist(utente);
	}
	
	/*------------------- READ --------------------*/
	@Transactional(readOnly=true)
	public List<Utente> findAll(){
		return entityManager.createQuery("FROM Utente", Utente.class).getResultList();
	}
	
	@Transactional(readOnly=true)
	public Utente findByID(long id){
		Utente utente = entityManager.find(Utente.class, id);
		if(utente==null)
			logger.warn("Id non presente");
		return utente;
	}
	
	/*------------------- UPDATE --------------------*/
	@Transactional(readOnly=false)
	public void updateUtente(Utente utente){
		entityManager.merge(utente);
	}
	
	/*------------------- DELETE --------------------*/
	@Transactional(readOnly=false)
	public void deleteUtente(long id){
		Utente utente =  this.findByID(id);
		entityManager.remove(utente);
	}
	
	/*------------------- UTILITIES --------------------*/
	@Transactional(readOnly=false)
	public void restart() {
		String sql = "ALTER SEQUENCE utente_utente_id_seq RESTART WITH 1";
		entityManager.createNativeQuery(sql).executeUpdate();
	}
	
	@Transactional
	public void clear(){
		entityManager.clear();
	}

}
