package it.anoki.model;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="azienda", uniqueConstraints={@UniqueConstraint(name="azienda_ragione_sociale_key",columnNames="ragione_sociale")})
@JsonInclude(Include.NON_NULL)
public class Azienda {
	
	
	/*------------------- FIELDS --------------------*/
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="azienda_id")
	private long id;
	
	@Column(name="ragione_sociale", nullable=false)
	private String ragioneSociale;
	
	@Column(name="indirizzo")
	private String indirizzo;
	
	@Column(name="codice_postale")
	private String codicePostale;
	
	@Column(name="citta")
	private String citta;
	
	@Column(name="stato")
	private String stato;
	
	@Column(name="orario_apertura")
	private Time orarioApertura;
	
	@Column(name="orario_chiusura")
	private Time orarioChiusura;
	
	@OneToMany(mappedBy="azienda")
	private List<Persona> persone;
	
	@OneToMany(mappedBy="azienda")
	private List<RecapitoAzienda> contatti;

	/*------------------- METHODS --------------------*/
	public Azienda(){
		persone = new ArrayList<Persona>();
		contatti = new ArrayList<RecapitoAzienda>();
	}

	public long getId() {
		return this.id;
	}
	
	public void setId(long id){
		this.id=id;
	}

	public String getRagioneSociale() {
		return ragioneSociale;
	}

	public void setRagioneSociale(String ragione_sociale) {
		this.ragioneSociale = ragione_sociale;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getCodicePostale() {
		return codicePostale;
	}

	public void setCodicePostale(String codicePostale) {
		this.codicePostale = codicePostale;
	}

	public String getCitta() {
		return citta;
	}

	public void setCitta(String citta) {
		this.citta = citta;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public Time getOrarioApertura() {
		return orarioApertura;
	}

	public void setOrarioApertura(Time orarioApertura) {
		this.orarioApertura = orarioApertura;
	}

	public Time getOrarioChiusura() {
		return orarioChiusura;
	}

	public void setOrarioChiusura(Time orarioChiusura) {
		this.orarioChiusura = orarioChiusura;
	}
	
	@JsonIgnore
	public List<Persona> getPersone() {
		return persone;
	}

	public void setPersone(List<Persona> persone) {
		this.persone = persone;
	}
	
	public boolean addPersona(Persona persona){
		return persone.add(persona);
	}

	@JsonIgnore
	public List<RecapitoAzienda> getContatti() {
		return contatti;
	}

	public void setContatti(List<RecapitoAzienda> contatti) {
		this.contatti = contatti;
	}
	
	@Override
	public String toString(){
		return "[" + this.id + ", " 
				+ this.ragioneSociale + ", "
				+ this.indirizzo + ", "
				+ this.citta + ", "
				+ this.codicePostale + ", "
				+ this.stato + "]";
	}
	
	@Override
	public boolean equals(Object o){
		Azienda a = (Azienda)o;
		return 
				id==a.getId() && 
				ragioneSociale.equals(a.getRagioneSociale()) &&
				(indirizzo == null ? a.getIndirizzo()== null : indirizzo.equals(a.getIndirizzo())) &&
				(citta == null ? a.getCitta()== null : citta.equals(a.getCitta())) &&
				(codicePostale == null ? a.getCodicePostale()== null : 
								codicePostale.equals(a.getCodicePostale())) &&
				(stato == null ? a.getStato()== null : stato.equals(a.getStato())); 

	}
	
	
	
}
