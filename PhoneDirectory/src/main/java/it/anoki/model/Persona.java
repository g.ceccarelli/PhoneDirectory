package it.anoki.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="persona", uniqueConstraints={
		@UniqueConstraint(name="persona_nome_cognome_key",columnNames={"cognome","nome"}),
		@UniqueConstraint(name="persona_email_key", columnNames="email")

	})
@JsonInclude(Include.NON_NULL)
public class Persona {
	
	/*------------------- FIELDS --------------------*/

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="persona_id")
	private long id;
	
	@Column(name="cognome")
	private String cognome;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="profilo")
	private String profilo;
	
	@Column(name="email",nullable=false)
	private String email;
	
	@ManyToOne
	@JoinColumn(name="azienda_id", nullable=false, foreignKey=@ForeignKey(name="persona_azienda_id_fkey"))
	private Azienda azienda;
	
	@OneToOne(mappedBy="persona")
	private Utente utente;
	
	@OneToMany(mappedBy="persona")
	private Set<RecapitoPersona> contatti;
	
	/*------------------- METHODS --------------------*/
	
	public Persona(){
		contatti = new HashSet<RecapitoPersona>();
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getProfilo() {
		return profilo;
	}

	public void setProfilo(String profilo) {
		this.profilo = profilo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonIgnoreProperties({"indirizzo", "stato", "citta", 
		"codicePostale", "orarioApertura", "orarioChiusura"})
	public Azienda getAzienda() {
		return azienda;
	}

	public void setAzienda(Azienda azienda) {
		this.azienda = azienda;
	}
	
	@JsonIgnore
	public Utente getUtente() {
		return utente;
	}

	public void setUtente(Utente utente) {
		this.utente = utente;
	}
	
	@JsonIgnore
	public Set<RecapitoPersona> getContatti() {
		return contatti;
	}

	public void setContatti(Set<RecapitoPersona> contatti) {
		this.contatti = contatti;
	}
	
	public void addContatto(RecapitoPersona recapito){
		this.contatti.add(recapito);
	}
	
	@Override
	public String toString(){
		return "[" + this.id + ", "
				+ this.cognome +", "
				+ this.nome + ", "
				+ this.email + ", "
				+ this.azienda.getRagioneSociale() + "]";
	}
	
	@Override
	public boolean equals(Object o){
		Persona p = (Persona)o;
		return 	
				id==(p.getId()) && 
				email.equals(p.getEmail()) && 
				azienda.equals(p.getAzienda()) &&
				(nome==null ? p.getNome()==null : nome.equals(p.getNome())) &&
				(cognome==null ? p.getCognome()==null : cognome.equals(p.getCognome())) &&
				(profilo==null ? p.getProfilo()== null : this.profilo.equals(p.getProfilo()));
	}
}
