package it.anoki.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="recapito_azienda", uniqueConstraints={@UniqueConstraint(name="unique_azienda_descrizione",columnNames={"azienda_id","descrizione"})})
@JsonInclude(Include.NON_NULL)
public class RecapitoAzienda {
	
	/*------------------- FIELDS --------------------*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="recapito_azienda_id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name="azienda_id", nullable=false, foreignKey=@ForeignKey(name="recapito_azienda_azienda_id_fkey"))
	private Azienda azienda;
	
	@OneToOne
	@JoinColumn(name="telefono_id", nullable=false, unique=true, foreignKey=@ForeignKey(name="recapito_azienda_telefono_id_fkey"))
	private Telefono telefonoAzienda;
	
	private int voti;
	private boolean stato = true;
	@Column(nullable=false)
	private String descrizione;
	
	/*------------------- METHODS --------------------*/
	public RecapitoAzienda(){}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@JsonIgnoreProperties({"indirizzo", "codicePostale", "citta", 
		"orarioApertura", "orario_chiusura", "stato"})
	public Azienda getAzienda() {
		return azienda;
	}
	public void setAzienda(Azienda azienda) {
		this.azienda = azienda;
	}
	
//	@JsonIgnoreProperties({"id"})
	public Telefono getTelefonoAzienda() {
		return telefonoAzienda;
	}
	public void setTelefonoAzienda(Telefono telefono) {
		this.telefonoAzienda = telefono;
	}
	
	public int getVoti() {
		return this.voti;
	}
	public void setVoti(int voti) {
		this.voti = voti;
	}
	
	public void up(){
		this.voti += 1;
	}
	
	public void down(){
		this.voti -= 1;
	}
	
	public boolean getStato() {
		return stato;
	}
	public void setStato(boolean stato) {
		this.stato = stato;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	@Override
	public String toString(){
		return 	"[" + this.getId() + ", "
				+ this.getAzienda().getRagioneSociale() + ", "
				+ this.getTelefonoAzienda().getNumero() +", "
				+ this.getDescrizione() + "]";
	}
	
	@Override
	public boolean equals(Object o){
		RecapitoAzienda recapito = (RecapitoAzienda)o;
		return this.id==recapito.getId() &&
				this.azienda.equals(recapito.getAzienda()) &&
				this.telefonoAzienda.equals(recapito.getTelefonoAzienda()) &&
				this.descrizione.equals(recapito.getDescrizione()) &&
				this.stato == recapito.getStato() &&
				this.voti == recapito.getVoti();
	}
	

}
