package it.anoki.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="recapito_persona", uniqueConstraints={@UniqueConstraint(name="unique_recapito",columnNames={"persona_id","telefono_id","descrizione"})})
@JsonInclude(Include.NON_NULL)
public class RecapitoPersona {
	
	/*------------------- FIELDS --------------------*/

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="recapito_persona_id")
	private long id;
	
	@ManyToOne
	@JoinColumn(name="persona_id", nullable=false, foreignKey=@ForeignKey(name="recapito_persona_persona_id_fkey"))
	private Persona persona;
	
	@ManyToOne
	@JoinColumn(name="telefono_id", nullable = false, foreignKey=@ForeignKey(name="recapito_persona_telefono_id_fkey"))
	private Telefono telefonoPersona;
	
	private int voti;
	private boolean stato = true;
	@Column(name="descrizione", nullable=false)
	private String descrizione;
	
	/*------------------- METHODS --------------------*/
	public RecapitoPersona(){}

	public long getId() {
		return id;
	}

	public void setIdd(long id) {
		this.id = id;
	}

	public int getVoti() {
		return voti;
	}
	
	public void up(){
		this.voti += 1;
	}
	
	public void down(){
		this.voti -=1;
	}

	public void setVoti(int voti) {
		this.voti = voti;
	}

	public boolean getStato() {
		return stato;
	}

	public void setStato(boolean stato) {
		this.stato = stato;
	}
	
	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@JsonIgnoreProperties({"mail","profilo","azienda","utente"})
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	public Telefono getTelefonoPersona() {
		return telefonoPersona;
	}

	public void setTelefonoPersona(Telefono telefono) {
		this.telefonoPersona = telefono;
	}
	
	@Override
	public String toString(){
		return 	"[" + this.getId() + ", "
				+ this.getPersona().getEmail() + ", "
				+ this.getTelefonoPersona().getNumero() +", "
				+ this.getDescrizione() + "]";
	}
	
	@Override
	public boolean equals(Object o){
		RecapitoPersona p = (RecapitoPersona)o;
		return 
				this.getId()==p.getId() &&
				this.getPersona().equals(p.getPersona()) &&
				this.getTelefonoPersona().equals(p.getTelefonoPersona()) &&
				this.getDescrizione().equals(p.getDescrizione()) &&
				this.stato == p.getStato() &&
				this.voti == p.getVoti();
	}
	
}
