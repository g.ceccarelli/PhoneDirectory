package it.anoki.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="telefono",uniqueConstraints={@UniqueConstraint(name="telefono_unique",columnNames="telefono")})
@JsonInclude(Include.NON_NULL)
public class Telefono {

	/*------------------- FIELDS --------------------*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="telefono_id")
	private long id;
	
	@Column(name="tipo")
	private String tipo;
	
	@Column(name="telefono", nullable=false)
	private String numero;
	
	@OneToMany(mappedBy="telefonoPersona")
	private Set<RecapitoPersona> recapitoPersona;
	
	@OneToOne(mappedBy="telefonoAzienda")
	private RecapitoAzienda recapitoAzienda;
	
	/*------------------- METHODS --------------------*/
	public Telefono(){
		recapitoPersona = new HashSet<RecapitoPersona>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTipo() {
		return this.tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@JsonIgnore
	public Set<RecapitoPersona> getRecapitoPersona() {
		return recapitoPersona;
	}

	public void setRecapitoPersona(Set<RecapitoPersona> recapitoPersona) {
		this.recapitoPersona = recapitoPersona;
	}
	
	public void addRecapitoPersona(RecapitoPersona p){
		this.recapitoPersona.add(p);
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	@JsonIgnore
	public RecapitoAzienda getRecapitoAzienda() {
		return recapitoAzienda;
	}

	public void setRecapitoAzienda(RecapitoAzienda recapitoAzienda) {
		this.recapitoAzienda = recapitoAzienda;
	}
	
	@Override
	public boolean equals(Object o){
		Telefono t = (Telefono)o;
		return this.getId()==t.getId() &&
				this.getNumero().equals(t.getNumero()) &&
				(tipo==null ? t.getTipo()==null : tipo.equals(t.getTipo()));

	}
	
}
