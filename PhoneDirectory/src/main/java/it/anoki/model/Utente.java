package it.anoki.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@Table(name="utente",uniqueConstraints={@UniqueConstraint(name="unique_persona",columnNames="persona_id")})
@JsonInclude(Include.NON_NULL)
public class Utente {
	
	/*------------------- FIELDS --------------------*/
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="utente_id")
	private long id;
	
	@OneToOne
	@JoinColumn(name="persona_id", foreignKey=@ForeignKey(name="utente_persona_id_fkey"), nullable=false)
	private Persona persona;
	
	@Column(name="password", nullable=false)
	private String password;

	/*------------------- METHODS --------------------*/
	public long getId() {
		return this.id;
	}
	
	public void setId(long id){
		this.id=id;
	}

	@JsonIgnoreProperties({"id", "cognome", "nome", "profilo"
		,"azienda", "contatti"})
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public boolean equals(Object o){
		Utente u = (Utente)o;
		return this.getId()==u.getId() &&
				this.getPersona().equals(u.getPersona()) &&
				this.getPassword().equals(u.getPassword());
	}
	
}
