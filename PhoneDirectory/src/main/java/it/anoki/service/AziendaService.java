package it.anoki.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.dao.AziendaDAO;
import it.anoki.model.Azienda;
import it.anoki.model.Persona;
import it.anoki.model.RecapitoAzienda;
import it.anoki.model.RecapitoPersona;

@Service
public class AziendaService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Transactional(readOnly=true)
	public List<RecapitoAzienda> getRecapitiAzienda(String ragioneSociale){
		Azienda azienda = aziendaDAO.findByRagioneSociale(ragioneSociale);
		if(azienda==null){
			logger.warn("Cannot find ragioneSociale");
			return null;
		}else
			return azienda.getContatti();
	}
	
	@Transactional(readOnly=true)
	public List<RecapitoPersona> getRecapitiDipendenti(String ragioneSociale){
		
		List<RecapitoPersona> contatti = new ArrayList<RecapitoPersona>();
		Azienda azienda = aziendaDAO.findByRagioneSociale(ragioneSociale);
		if(azienda!=null){
			for(Persona persona : azienda.getPersone()){
				if(!(persona.getContatti().isEmpty())){
					for(RecapitoPersona recapito : persona.getContatti()){
						contatti.add(recapito);
					}
				}
			}
		}else
			contatti = null;
		
		return contatti;
	}
	
	
}
