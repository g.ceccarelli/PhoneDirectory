package it.anoki.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.dao.PersonaDAO;
import it.anoki.model.Persona;

@Service
public class PersonaService {
		
	@Autowired
	PersonaDAO personaDAO;
	
	@Transactional(readOnly=true)
	public boolean findDuplicateName(String nome, String cognome){
		ArrayList<Persona> persona = (ArrayList<Persona>) personaDAO.findAll();
		for(Persona p : persona){
			if(p.getCognome().equals(cognome) && p.getNome().equals(nome)){
				return true;
			}
		}
		return false;
	}
	
	@Transactional(readOnly=true)
	public boolean findDuplicateEmail(String email){
		Persona persona = personaDAO.findByEmail(email);
		if(persona==null){
			return false;
		}else{
			return true;
		}
	}
	
	


}
