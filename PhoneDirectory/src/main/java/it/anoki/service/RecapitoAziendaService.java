package it.anoki.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.dao.RecapitoAziendaDAO;
import it.anoki.model.RecapitoAzienda;

@Service
public class RecapitoAziendaService {
	
	@Autowired
	RecapitoAziendaDAO recapitoAziendaDAO;
	
	@Transactional(readOnly=false)
	public void up(RecapitoAzienda recapitoAzienda){
		RecapitoAzienda recapito = recapitoAziendaDAO.findById(recapitoAzienda.getId());
		recapito.up();
		if(recapito.getVoti()<=-2)
			recapito.setStato(false);
		else{
			if(recapito.getVoti()>-2)
				recapito.setStato(true);
		}
		recapitoAziendaDAO.updateRecapitoAzienda(recapito);
	}
	
	@Transactional(readOnly=false)
	public void down(RecapitoAzienda recapitoAzienda){
		RecapitoAzienda recapito = recapitoAziendaDAO.findById(recapitoAzienda.getId());
		recapito.down();
		if(recapito.getVoti()<=-2)
			recapito.setStato(false);
		else{
			if(recapito.getVoti()>-2)
				recapito.setStato(true);
		}
		recapitoAziendaDAO.updateRecapitoAzienda(recapito);
	}

}
