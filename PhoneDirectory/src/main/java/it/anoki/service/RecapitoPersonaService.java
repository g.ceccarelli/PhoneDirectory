package it.anoki.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.dao.RecapitoPersonaDAO;
import it.anoki.dao.TelefonoDAO;
import it.anoki.model.RecapitoPersona;

@Service
public class RecapitoPersonaService {
	
	@Autowired
	TelefonoDAO telefonoDAO;
	
	@Autowired
	RecapitoPersonaDAO recapitoPersonaDAO;
	
	@Transactional(readOnly=false)
	public void up(RecapitoPersona recapitoPersona){
		RecapitoPersona recapito = recapitoPersonaDAO.findById(recapitoPersona.getId());
		recapito.up();
		if(recapito.getVoti()<=-2)
			recapito.setStato(false);
		else{
			if(recapito.getVoti()>-2)
				recapito.setStato(true);
		}
		recapitoPersonaDAO.updateRecapitoPersona(recapito);
	}
	
	@Transactional(readOnly=false)
	public void down(RecapitoPersona recapitoPersona){
		RecapitoPersona recapito = recapitoPersonaDAO.findById(recapitoPersona.getId());
		recapito.down();
		if(recapito.getVoti()<=-2)
			recapito.setStato(false);
		else{
			if(recapito.getVoti()>-2)
				recapito.setStato(true);
		}
		recapitoPersonaDAO.updateRecapitoPersona(recapito);
	}

}
