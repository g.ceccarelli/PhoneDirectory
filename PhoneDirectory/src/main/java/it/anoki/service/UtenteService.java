package it.anoki.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import DTO.UtenteDTO;
import it.anoki.dao.PersonaDAO;
import it.anoki.dao.UtenteDAO;
import it.anoki.model.Persona;
import it.anoki.model.Utente;

@Service
public class UtenteService {
	
	@Autowired
	UtenteDAO utenteDAO;
	
	@Autowired
	PersonaDAO personaDAO;
	
	@Transactional
	public boolean login(UtenteDTO utenteDTO){	
		
		Boolean ret = false;
		Persona persona = personaDAO.findByEmail(utenteDTO.getMail());
		if(persona!= null){
			Utente utente = persona.getUtente();
			if(utente!=null && utente.getPassword().equals(utenteDTO.getPassword()))
				ret =  true;
		}
		return ret;
	}
	
}
