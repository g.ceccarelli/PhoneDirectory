package it.anoki.controller;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import com.fasterxml.jackson.databind.ObjectMapper;

import it.anoki.dao.PersonaDAO;
import it.anoki.model.Azienda;
import it.anoki.model.Persona;

@RunWith(SpringRunner.class)
@WebMvcTest(value=PersonaController.class)
public class PersonaControllerTests {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private PersonaDAO personaDAO;
	
	@Autowired
	ObjectMapper mapper;
	
	Persona persona1;
	Persona persona2;
	Persona persona3;
	
	
	@Before
	public void setup(){
		persona1 = new Persona();
		persona1.setId(1);
		persona1.setCognome("cognome1");
		persona1.setNome("nome1");
		persona1.setEmail("email@gmail.com");
		Azienda azienda = new Azienda();
		azienda.setId(1);
		persona1.setAzienda(azienda);
		
		persona2 = new Persona();
		persona2.setId(2);
		persona2.setCognome("cognome2");
		persona2.setNome("nome2");
	}

	
	@Test
	public void getPersone() throws Exception{
		Mockito.when(personaDAO.findAll()).thenReturn(Arrays.asList(persona1, persona2));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.	get("/persona/")
				.accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		String expected = "[{id:1,cognome:cognome1,nome:nome1},{id:2,cognome:cognome2,nome:nome2}]";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
	
	
	@Test
	public void getPersonaId() throws Exception{
		Mockito.when(personaDAO.findByID(Mockito.anyLong())).thenReturn(persona1);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/persona/id/1").accept
				(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		String expected = "{id:1,cognome:cognome1,nome:nome1,email:email@gmail.com}";
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void getPersonaEmail() throws Exception{
		Mockito.when(personaDAO.findByEmail(Mockito.anyString())).thenReturn(persona1);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/persona/email/email@gmail.com").accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		String expected = "{id:1,cognome:cognome1,nome:nome1}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
	
	@Test
	public void insertPersona() throws Exception{
		ObjectMapper obj = new ObjectMapper();
		String persona = obj.writeValueAsString(persona1);
		
		Mockito.when(personaDAO.createPersona(Mockito.any(Persona.class))).thenReturn(persona1);
		//String exampleCourseJson = "{\"id\":\"2\",\"cognome\":\"cognome1\",\"nome\":\"nome1\"}";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/persona/")
				.accept(MediaType.APPLICATION_JSON_UTF8)
				.content(persona)
				.contentType(MediaType.APPLICATION_JSON_UTF8);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		String expected = "{id:1,cognome:cognome1,nome:nome1,email:email@gmail.com,azienda:{id:1}}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);

	}
//	
//	@Test
//	public void updatePersona() throws Exception{
//		ObjectMapper obj = new ObjectMapper();
//		String persona = obj.writeValueAsString(persona1);
//		
//		Mockito.when(personaDAO.updatePersona(Mockito.any(Persona.class))).thenReturn(persona1);
//		
//	}
//	
	
	
}
