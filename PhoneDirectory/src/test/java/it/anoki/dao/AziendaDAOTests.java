package it.anoki.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Azienda;


@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Sql(scripts="AziendaDAOTests.sql")


public class AziendaDAOTests {
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	
	/*-------------------------------------------ADD-------------------------------------------*/
	@Test
	public void testAddAzienda(){
		Azienda azienda = new Azienda();
		azienda.setRagioneSociale("adidas");
		azienda.setCitta("Milano");
		azienda.setCodicePostale("20090");
		azienda.setIndirizzo("via scarpa");
		Assert.assertEquals(azienda, aziendaDAO.createAzienda(azienda));
	}
	
	@Test
	public void testAddAziendaError(){
		Azienda azienda = new Azienda();
		azienda.setCodicePostale("22");
		azienda.setIndirizzo("aaaa");
		Assert.assertNull(aziendaDAO.createAzienda(azienda));
	}
	
	@Test(expected=DataIntegrityViolationException.class)
	public void testAddAziendaUnique(){
		Azienda azienda = new Azienda();
		azienda.setRagioneSociale("anoki");
		aziendaDAO.createAzienda(azienda);
		
		Azienda azienda1 = new Azienda();
		azienda1.setRagioneSociale("anoki");
		aziendaDAO.createAzienda(azienda1);
	}
	
	/*-------------------------------------------FIND-------------------------------------------*/
	@Test
	public void testFindById(){
		Azienda azienda = aziendaDAO.findByRagioneSociale("anoki");
		Assert.assertEquals(azienda, aziendaDAO.findById(1));
	}
	
	@Test
	public void testFindByIdError(){
		Assert.assertNull(aziendaDAO.findById(22));
	}
	
	@Test
	public void testFindAll(){
		System.err.println(aziendaDAO.findAll());
		Assert.assertEquals(2, aziendaDAO.findAll().size());
	}
	
	@Test
	public void testFindAllEmpty(){
		aziendaDAO.delete(1);
		aziendaDAO.delete(2);
		Assert.assertTrue(aziendaDAO.findAll().isEmpty());
	}
	
	@Test
	@Transactional
	public void testFindAllDelete(){
		aziendaDAO.delete(1);
		Assert.assertEquals(1, aziendaDAO.findAll().size());
	}
	
	@Test
	public void testFindByRagioneSociale(){
		Azienda azienda = aziendaDAO.findById(1);
		Assert.assertEquals(azienda, aziendaDAO.findByRagioneSociale("anoki"));
	}
	
	@Test
	@Transactional
	public void testFindByRagioneSocialeError(){
		Assert.assertNull(aziendaDAO.findByRagioneSociale("ciao"));
	}
	
	/*-------------------------------------------DELETE-------------------------------------------*/
	@Test
	public void testDelete(){
		aziendaDAO.delete(1);
		Assert.assertNull(aziendaDAO.findByRagioneSociale("anoki"));
	}
	
	@Test
	public void testDeleteError(){
		aziendaDAO.delete(22);
	}
	
	/*-------------------------------------------UPDATE-------------------------------------------*/
	@Test
	public void testUpdate(){
		Azienda azienda = aziendaDAO.findByRagioneSociale("anoki");
		azienda.setIndirizzo("via gioia");
		aziendaDAO.updateAzienda(1, azienda);
		Assert.assertEquals(azienda, aziendaDAO.findById(1));
	}


}
