package it.anoki.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Persona;

import java.util.Arrays;

import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@Sql(scripts="personaDAOTests.sql")

public class PersonaDAOTests {
	
	@Autowired
	PersonaDAO personaDAO;
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	/*-------------------------------------------ADD-------------------------------------------*/
	@Test
	public void testAddPersona(){
		Persona persona = new Persona();
		persona.setCognome("cognome");
		persona.setNome("nome");
		persona.setEmail("cognome.nome@gmail.com");
		persona.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		Assert.assertEquals(persona, personaDAO.createPersona(persona));
		}
	
	@Test
	@Transactional
	public void testAddPersonaErrorAzienda(){
		Persona persona = new Persona();
		persona.setCognome("prova22");
		persona.setNome("prova22");
		persona.setEmail("prova");
		Assert.assertNull(personaDAO.createPersona(persona));
	}
	
	@Test
	public void testAddPersonaErrorEmailNull(){
		//Not Null constraint error
		Persona persona1 = new Persona();
		persona1.setCognome("cognome");
		persona1.setNome("nome");
		Assert.assertNull(personaDAO.createPersona(persona1));
	}
	
	@Test(expected=DataIntegrityViolationException.class)
	public void testAddPersonaUniqueError(){
		Persona persona = new Persona();
		persona.setCognome("pippo");
		persona.setNome("pluto");
		persona.setEmail("pippo.pluto@mail.com");
		persona.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		personaDAO.createPersona(persona);
	}
	/*-------------------------------------------FIND-------------------------------------------*/
	
	@Test
	public void testFindID(){
		Persona persona = personaDAO.findByEmail("pippo.pluto@mail.com");
		Assert.assertEquals(persona, personaDAO.findByID(1));
	}
	
	@Test
	public void testFindIdError(){
		Assert.assertNull(personaDAO.findByID(22));
	}
	
	@Test
	public void testFindAll(){
		Persona persona1 = personaDAO.findByID(1);
		Persona persona2 = personaDAO.findByID(2);
		Persona persona3 = personaDAO.findByID(3);

		Assert.assertEquals(Arrays.asList(persona1,persona2, persona3), personaDAO.findAll());
	}
	
	@Test
	public void testFindAllError(){
		personaDAO.deletePersona(1);
		personaDAO.deletePersona(2);
		personaDAO.deletePersona(3);
		Assert.assertTrue(personaDAO.findAll().isEmpty());
	}
	
	@Test
	public void testFindAllDelete(){
		Persona persona1 = personaDAO.findByID(1);
		personaDAO.deletePersona(2);
		Persona persona2 = personaDAO.findByID(3);
		Assert.assertEquals(Arrays.asList(persona1, persona2), personaDAO.findAll());
	}
	
	@Test
	public void testFindByEmail(){
		Persona persona = personaDAO.findByID(1);
		Assert.assertEquals(persona, personaDAO.findByEmail("pippo.pluto@mail.com"));
	}
	
	@Test
	public void testByEmailError(){
		Assert.assertNull(personaDAO.findByEmail("nonesiste"));
	}
	
	
	/*-------------------------------------------DELETE-------------------------------------------*/
	@Test
	public void testDelete(){
		personaDAO.deletePersona(1);
		Assert.assertNull(personaDAO.findByID(1));
	}
	
	@Test(expected=NullPointerException.class)
	public void testDeleteError(){
		personaDAO.deletePersona(22);
	}
	
	/*-------------------------------------------UPDATE-------------------------------------------*/
	
	@Test
	public void testUpdate(){
		Persona persona = personaDAO.findByID(1);
		persona.setCognome("cognomeCambio");
		personaDAO.updatePersona(persona);
		
		Assert.assertEquals("cognomeCambio", personaDAO.findByID(1).getCognome());
	}
	

}
