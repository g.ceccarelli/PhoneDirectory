package it.anoki.dao;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Azienda;
import it.anoki.model.RecapitoAzienda;
import it.anoki.model.Telefono;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RecapitoAziendaDAOTests {
	
	@Autowired
	RecapitoAziendaDAO recapitoAziendaDAO;
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Autowired
	TelefonoDAO telefonoDAO;
	
	@Before
	public void setUp(){
		Azienda azienda = new Azienda();
		azienda.setRagioneSociale("anoki");
		aziendaDAO.createAzienda(azienda);
		
		Telefono telefono = new Telefono();
		telefono.setNumero("1111");
		telefonoDAO.createTelefono(telefono);
		
		Telefono telefono1 = new Telefono();
		telefono1.setNumero("1234");
		telefonoDAO.createTelefono(telefono1);
	}
	
	/*-------------------------------------------ADD-------------------------------------------*/	
	@Test
	@Transactional
	public void addRecapitoAzienda(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("ufficio");
		Assert.assertEquals(recapitoAzienda, recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda));
		System.err.println(recapitoAziendaDAO.findAll());
	}
	
	@Test
	@Transactional
	public void addRecapitoAziendaNullAzienda(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("ufficio");
		Assert.assertNull(recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda));
	}
	
	@Test
	@Transactional
	public void addRecapitoAziendaNullTelefono(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		Assert.assertNull(recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda));
	}
	
	@Test(expected=DataIntegrityViolationException.class)
	@Transactional
	public void addRecapitoAziendaUnique(){
		RecapitoAzienda recapitoAzienda1 = new RecapitoAzienda();
		recapitoAzienda1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda1.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda1.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda1);
		
		RecapitoAzienda recapitoAzienda2 = new RecapitoAzienda();
		recapitoAzienda2.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda2.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda2.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda2);
	}
	
	@Test(expected=DataIntegrityViolationException.class)
	@Transactional
	public void addRecapitoAziendaNotUnique(){
		RecapitoAzienda recapitoAzienda1 = new RecapitoAzienda();
		recapitoAzienda1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda1.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda1.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda1);
		
		RecapitoAzienda recapitoAzienda2 = new RecapitoAzienda();
		recapitoAzienda2.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda2.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda2.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda2);
		
		Assert.assertEquals(Arrays.asList(recapitoAzienda1, recapitoAzienda2), recapitoAziendaDAO.findAll());
	}
	
	/*-------------------------------------------FIND-------------------------------------------*/
	@Test
	@Transactional
	public void findById(){
		RecapitoAzienda recapitoAzienda1 = new RecapitoAzienda();
		recapitoAzienda1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda1.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda1.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda1);
		
		Assert.assertEquals(recapitoAzienda1, recapitoAziendaDAO.findById(recapitoAzienda1.getId()));
	}
	
	@Test
	@Transactional
	public void findByIdError(){
		Assert.assertNull(recapitoAziendaDAO.findById(11));
	}
	
	@Test
	@Transactional
	public void findAll(){
		RecapitoAzienda recapitoAzienda1 = new RecapitoAzienda();
		recapitoAzienda1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda1.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda1.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda1);
		
		RecapitoAzienda recapitoAzienda2 = new RecapitoAzienda();
		recapitoAzienda2.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda2.setTelefonoAzienda(telefonoDAO.findByNumero("1234"));
		recapitoAzienda2.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda2);
		
		Assert.assertEquals(Arrays.asList(recapitoAzienda1, recapitoAzienda2), recapitoAziendaDAO.findAll());
	}
	
	@Test
	@Transactional
	public void finAllEmpty(){
		Assert.assertTrue(recapitoAziendaDAO.findAll().isEmpty());
	}
	
	/*-------------------------------------------UPDATE-------------------------------------------*/

	@Test
	@Transactional
	public void update(){
		RecapitoAzienda recapitoAzienda1 = new RecapitoAzienda();
		recapitoAzienda1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda1.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda1.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda1);
		
		RecapitoAzienda recapitoAzienda2 = recapitoAziendaDAO.findById(recapitoAzienda1.getId());
		recapitoAzienda2.setDescrizione("amministrazione");
		recapitoAziendaDAO.updateRecapitoAzienda(recapitoAzienda2);
		
		Assert.assertEquals(recapitoAzienda2, recapitoAziendaDAO.findById(recapitoAzienda1.getId()));
	}
	
	@Test
	@Transactional
	public void updateError(){
		RecapitoAzienda recapitoAzienda1 = new RecapitoAzienda();
		recapitoAzienda1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda1.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda1.setDescrizione("ufficio");
		
		Assert.assertNull(recapitoAziendaDAO.findById(recapitoAzienda1.getId()));
	}
	
	/*-------------------------------------------DELETE-------------------------------------------*/
	@Test
	@Transactional
	public void delete(){
		RecapitoAzienda recapitoAzienda1 = new RecapitoAzienda();
		recapitoAzienda1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda1.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda1.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda1);
		
		recapitoAziendaDAO.deleteRecapitoAzienda(recapitoAzienda1.getId());
		
		Assert.assertNull(recapitoAziendaDAO.findById(recapitoAzienda1.getId()));
	}
	
	@Test
	@Transactional
	public void deleteError(){
		recapitoAziendaDAO.deleteRecapitoAzienda(11);
	}
	
	
	
	
	
	
	
	
	
}
