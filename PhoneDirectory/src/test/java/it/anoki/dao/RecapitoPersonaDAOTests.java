package it.anoki.dao;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Azienda;
import it.anoki.model.Persona;
import it.anoki.model.RecapitoPersona;
import it.anoki.model.Telefono;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RecapitoPersonaDAOTests {
	
	@Autowired
	RecapitoPersonaDAO recapitoPersonaDAO;
	
	@Autowired
	PersonaDAO personaDAO;
	
	@Autowired
	TelefonoDAO telefonoDAO;
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Before
	public void setUp(){
		Persona persona = new Persona();
		persona.setEmail("prova");
		Azienda azienda= new Azienda();
		azienda.setRagioneSociale("anoki");
		persona.setAzienda(aziendaDAO.createAzienda(azienda));
		personaDAO.createPersona(persona);
		
		Telefono telefono = new Telefono();
		telefono.setNumero("0");
		telefonoDAO.createTelefono(telefono);
		
	}
	/*-------------------------------------------ADD-------------------------------------------*/
	@Test
	@Transactional
	public void addRecapitoPersona(){
		RecapitoPersona recapitoPersona = new RecapitoPersona();
		recapitoPersona.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona.setDescrizione("ciao");
		Assert.assertEquals(recapitoPersona, recapitoPersonaDAO.createRecapitoPersona(recapitoPersona));
	}
	
	
	@Test(expected=DataIntegrityViolationException.class)
	@Transactional
	public void addRecapitoUnique(){
		RecapitoPersona recapitoPersona1 = new RecapitoPersona();
		recapitoPersona1.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona1.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona1.setDescrizione("ciao");
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona1);
		
		RecapitoPersona recapitoPersona2 = new RecapitoPersona();
		recapitoPersona2.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona2.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona2.setDescrizione("ciao");
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona2);
		
	}
	
	@Test
	@Transactional
	public void addRecapitPersonaNullPersona(){
		RecapitoPersona recapitoPersona = new RecapitoPersona();
		recapitoPersona.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona.setDescrizione("ciao");
		Assert.assertNull(recapitoPersonaDAO.createRecapitoPersona(recapitoPersona));
	}
	
	@Test
	@Transactional
	public void addRecapitPersonaNullTelefono(){
		RecapitoPersona recapitoPersona = new RecapitoPersona();
		recapitoPersona.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona.setDescrizione("ciao");
		Assert.assertNull(recapitoPersonaDAO.createRecapitoPersona(recapitoPersona));
	}

	@Test
	@Transactional
	public void addRecapitPersonaNullDescrizione(){
		RecapitoPersona recapitoPersona = new RecapitoPersona();
		recapitoPersona.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		Assert.assertNull(recapitoPersonaDAO.createRecapitoPersona(recapitoPersona));
	}
	/*-------------------------------------------FIND-------------------------------------------*/
	@Test
	@Transactional
	public void findById(){
		RecapitoPersona recapitoPersona = new RecapitoPersona();
		recapitoPersona.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona.setDescrizione("ciao");
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona);
		Assert.assertEquals(recapitoPersona, recapitoPersonaDAO.findById(recapitoPersona.getId()));
	}
	
	public void findByIdError(){
		Assert.assertNull(recapitoPersonaDAO.findById(22));
	}
	
	@Test
	@Transactional
	public void findAll(){
		RecapitoPersona recapitoPersona1 = new RecapitoPersona();
		recapitoPersona1.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona1.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona1.setDescrizione("ciao");
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona1);
		
		RecapitoPersona recapitoPersona2 = new RecapitoPersona();
		recapitoPersona2.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona2.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona2.setDescrizione("aaa");
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona2);
		
		Assert.assertEquals(Arrays.asList(recapitoPersona1, recapitoPersona2), recapitoPersonaDAO.findAll());
	}
	
	@Test
	@Transactional
	public void findAllEmpty(){
		Assert.assertTrue(recapitoPersonaDAO.findAll().isEmpty());
	}
	
	/*-------------------------------------------UPDATE-----------------------------------------*/
	@Test
	@Transactional
	public void update(){
		RecapitoPersona recapitoPersona1 = new RecapitoPersona();
		recapitoPersona1.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona1.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona1.setDescrizione("ciao");
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona1);
		
		RecapitoPersona recapitoPersona2 = recapitoPersonaDAO.findById(recapitoPersona1.getId());
		recapitoPersona2.setDescrizione("nuova");
		recapitoPersonaDAO.updateRecapitoPersona(recapitoPersona2);
		
		Assert.assertEquals(recapitoPersona2, recapitoPersonaDAO.findById(recapitoPersona1.getId()));
	}
	
	@Test
	@Transactional
	public void updateError(){
		Assert.assertNull(recapitoPersonaDAO.findById(22));
	}
		
	/*-------------------------------------------DELETE-------------------------------------------*/
	@Test
	@Transactional
	public void delete(){
		RecapitoPersona recapitoPersona1 = new RecapitoPersona();
		recapitoPersona1.setTelefonoPersona(telefonoDAO.findByNumero("0"));
		recapitoPersona1.setPersona(personaDAO.findByEmail("prova"));
		recapitoPersona1.setDescrizione("ciao");
		recapitoPersonaDAO.createRecapitoPersona(recapitoPersona1);
		recapitoPersonaDAO.deleteRecapitoPersona(recapitoPersona1.getId());
		
		Assert.assertNull(recapitoPersonaDAO.findById(recapitoPersona1.getId()));
	}
	
	@Test
	@Transactional
	public void deleteError(){
		Assert.assertNull(recapitoPersonaDAO.findById(22));
	}


}
