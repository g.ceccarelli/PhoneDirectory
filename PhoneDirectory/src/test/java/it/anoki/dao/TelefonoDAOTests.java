package it.anoki.dao;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.model.Telefono;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TelefonoDAOTests {
	
	@Autowired
	TelefonoDAO telefonoDAO;
	
	/*-------------------------------------------ADD-------------------------------------------*/
	@Test
	@Transactional
	public void addTelefono(){
		Telefono telefono = new Telefono();
		telefono.setNumero("1234567");
		Assert.assertEquals(telefono, telefonoDAO.createTelefono(telefono));
	}
	
	@Test
	@Transactional
	public void addTelefonoError(){
		Telefono telefono = new Telefono();
		Assert.assertNull(telefonoDAO.createTelefono(telefono));
	}
	
	@Test(expected=DataIntegrityViolationException.class)
	@Transactional
	public void addTelefonoUnique(){
		Telefono telefono1 = new Telefono();
		telefono1.setNumero("1234567");
		telefonoDAO.createTelefono(telefono1);
		
		Telefono telefono2 = new Telefono();
		telefono2.setNumero("1234567");
		telefonoDAO.createTelefono(telefono2);
	}
	
	/*-------------------------------------------FIND-------------------------------------------*/
	@Test
	@Transactional
	public void findById(){
		Telefono telefono1 = new Telefono();
		telefono1.setNumero("1234567");
		telefonoDAO.createTelefono(telefono1);
		Assert.assertEquals(telefono1, telefonoDAO.findByID(telefono1.getId()));
	}
	
	@Test
	@Transactional
	public void findByIdError(){
		Assert.assertNull(telefonoDAO.findByID(22));
	}

	@Test
	@Transactional
	public void findAll(){
		Telefono telefono1 = new Telefono();
		telefono1.setNumero("12345");
		telefonoDAO.createTelefono(telefono1);
		
		Telefono telefono2 = new Telefono();
		telefono2.setNumero("67891");
		telefonoDAO.createTelefono(telefono2);
		
		Assert.assertEquals(Arrays.asList(telefono1, telefono2), telefonoDAO.findAll());
	}
	
	@Test
	@Transactional
	public void findAllError(){
		Assert.assertTrue(telefonoDAO.findAll().isEmpty());
	}
	
	@Test
	@Transactional
	public void findByNumbero(){
		Telefono telefono1 = new Telefono();
		telefono1.setNumero("12345");
		telefonoDAO.createTelefono(telefono1);
		Assert.assertEquals(telefono1, telefonoDAO.findByNumero("12345"));
	}
	
	@Test
	@Transactional
	public void findByNumberError(){
		Assert.assertNull(telefonoDAO.findByNumero("12345"));
	}
	
	/*-------------------------------------------DELETE-------------------------------------------*/
	@Test
	@Transactional
	public void delete(){
		Telefono telefono1 = new Telefono();
		telefono1.setNumero("12345");
		telefonoDAO.createTelefono(telefono1);
		telefonoDAO.deleteTelefono(telefono1.getId());
		Assert.assertNull(telefonoDAO.findByNumero("12345"));
	}
	
	/*-------------------------------------------UPDATE-------------------------------------------*/

	@Test
	@Transactional
	public void update(){
		Telefono telefono1 = new Telefono();
		telefono1.setNumero("12345");
		telefonoDAO.createTelefono(telefono1);
		
		Telefono telefono2 = telefonoDAO.findByID(telefono1.getId());
		telefono2.setNumero("67891");
		telefonoDAO.updateTelefono(telefono2);
		
		Assert.assertEquals(telefono2, telefonoDAO.findByID(telefono1.getId()));
	}

}
