package it.anoki.service;


import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.dao.AziendaDAO;
import it.anoki.dao.PersonaDAO;
import it.anoki.dao.RecapitoAziendaDAO;
import it.anoki.dao.RecapitoPersonaDAO;
import it.anoki.dao.TelefonoDAO;
import it.anoki.model.Azienda;
import it.anoki.model.Persona;
import it.anoki.model.RecapitoAzienda;
import it.anoki.model.RecapitoPersona;
import it.anoki.model.Telefono;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AziendaServiceTests {
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Autowired
	TelefonoDAO telefonoDAO;
	
	@Autowired
	RecapitoAziendaDAO recapitoAziendaDAO;
	
	@Autowired
	RecapitoPersonaDAO recapitoPersonaDAO;
	
	@Autowired
	PersonaDAO personaDAO;
	
	@Autowired
	AziendaService aziendaService;
	
	@Before
	public void setUp(){
		Azienda azienda = new Azienda();
		azienda.setRagioneSociale("anoki");
		aziendaDAO.createAzienda(azienda);
	}
	
	@Test
	@Transactional
	public void getRecapitiAzienda(){
		Telefono telefono = new Telefono();
		telefono.setNumero("1234");
		telefonoDAO.createTelefono(telefono);
		
		Telefono telefono2 = new Telefono();
		telefono2.setNumero("5678");
		telefonoDAO.createTelefono(telefono2);
		
		Telefono telefono3 = new Telefono();
		telefono3.setNumero("1");
		telefonoDAO.createTelefono(telefono3);
		
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1234"));
		recapitoAzienda.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
		
		RecapitoAzienda recapitoAzienda2 = new RecapitoAzienda();
		recapitoAzienda2.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda2.setTelefonoAzienda(telefonoDAO.findByNumero("5678"));
		recapitoAzienda2.setDescrizione("ufficio");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda2);
		
		RecapitoAzienda recapitoAzienda3 = new RecapitoAzienda();
		recapitoAzienda3.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda3.setTelefonoAzienda(telefonoDAO.findByNumero("1"));
		recapitoAzienda3.setDescrizione("nonloso");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda3);
		
		aziendaDAO.clear();
		
		Assert.assertEquals(Arrays.asList(recapitoAzienda, recapitoAzienda3, recapitoAzienda2),
				aziendaService.getRecapitiAzienda("anoki"));
	}
	
	@Test
	@Transactional
	public void getRecapitiAziendaEmpty(){
		Assert.assertTrue(aziendaService.getRecapitiAzienda("anoki").isEmpty());
	}
	
	@Test
	@Transactional
	public void getRecapitoAziendaEmptyError(){
		Assert.assertNull(aziendaService.getRecapitiAzienda("ciao"));
	}
	
	@Test
	@Transactional
	public void getRecapitiDipendenti(){
		Telefono telefono = new Telefono();
		telefono.setNumero("1234");
		telefonoDAO.createTelefono(telefono);
		
		Persona persona = new Persona();
		persona.setCognome("cognome");
		persona.setNome("nome");
		persona.setEmail("prova1");
		persona.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		personaDAO.createPersona(persona);
		
		Persona persona2 = new Persona();
		persona2.setCognome("cognome2");
		persona2.setNome("nome2");
		persona2.setEmail("prova2");
		persona2.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		personaDAO.createPersona(persona2);
		
		RecapitoPersona recapito = new RecapitoPersona();
		recapito.setPersona(persona);
		recapito.setTelefonoPersona(telefono);
		recapito.setDescrizione("numero1");
		recapitoPersonaDAO.createRecapitoPersona(recapito);
		
		RecapitoPersona recapito2 = new RecapitoPersona();
		recapito2.setPersona(persona2);
		recapito2.setTelefonoPersona(telefono);
		recapito2.setDescrizione("numero2");
		recapitoPersonaDAO.createRecapitoPersona(recapito2);
		
		aziendaDAO.clear();
		
		Assert.assertEquals(Arrays.asList(recapito, recapito2), aziendaService.getRecapitiDipendenti("anoki"));
	}
	
	@Test
	@Transactional
	public void getRecapitiDipendentiError(){
		Assert.assertNull(aziendaService.getRecapitiDipendenti("ciao"));
	}
	
	@Test
	@Transactional
	public void getRecapitiDipendentiEmpty(){
		Assert.assertTrue(aziendaService.getRecapitiAzienda("anoki").isEmpty());
	}
	
}
