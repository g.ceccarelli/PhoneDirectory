package it.anoki.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import DTO.UtenteDTO;
import it.anoki.dao.AziendaDAO;
import it.anoki.dao.PersonaDAO;
import it.anoki.dao.UtenteDAO;
import it.anoki.model.Azienda;
import it.anoki.model.Persona;
import it.anoki.model.Utente;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UtenteServiceTests {

	@Autowired
	UtenteDAO utenteDAO;
	
	@Autowired
	PersonaDAO personaDAO;
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Autowired
	UtenteService utenteService;
	
	@Before
	public void setUp(){
		Azienda azienda = new Azienda();
		azienda.setRagioneSociale("anoki");
		aziendaDAO.createAzienda(azienda);
		
		Persona persona = new Persona();
		persona.setEmail("prova@gmail.com");
		persona.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		personaDAO.createPersona(persona);
		
		Utente utente = new Utente();
		utente.setPersona(persona);
		utente.setPassword("password");
		utenteDAO.createUtente(utente);
		
		Persona persona1 = new Persona();
		persona1.setEmail("prova2@gmail.com");
		persona1.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		personaDAO.createPersona(persona1);
		
		
		Utente utente1 = new Utente();
		utente1.setPersona(persona1);
		utente1.setPassword("password");
		utenteDAO.createUtente(utente1);
		
		personaDAO.clear();

	}
	
	@Test
	@Transactional
	public void login(){
		UtenteDTO utenteDTO = new UtenteDTO();
		utenteDTO.setMail("prova@gmail.com");
		utenteDTO.setPassword("password");
		
		Assert.assertTrue(utenteService.login(utenteDTO));
	}
	
	@Test
	@Transactional
	public void loginErroPassword(){
		UtenteDTO utenteDTO = new UtenteDTO();
		utenteDTO.setMail("prova@gmail.com");
		utenteDTO.setPassword("prova");
		
		Assert.assertFalse(utenteService.login(utenteDTO));
	}
	
	@Test
	@Transactional
	public void loginError(){
		UtenteDTO utenteDTO = new UtenteDTO();		
		Assert.assertFalse(utenteService.login(utenteDTO));
	}
}
