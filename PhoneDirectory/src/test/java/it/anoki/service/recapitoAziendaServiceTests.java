package it.anoki.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import it.anoki.dao.AziendaDAO;
import it.anoki.dao.RecapitoAziendaDAO;
import it.anoki.dao.TelefonoDAO;
import it.anoki.model.Azienda;
import it.anoki.model.RecapitoAzienda;
import it.anoki.model.Telefono;

@RunWith(SpringRunner.class)
@SpringBootTest
public class recapitoAziendaServiceTests {

	@Autowired
	RecapitoAziendaService recapitoAziendaService;
	
	@Autowired
	AziendaDAO aziendaDAO;
	
	@Autowired
	TelefonoDAO telefonoDAO;
	
	@Autowired
	RecapitoAziendaDAO recapitoAziendaDAO;
	
	@Before
	public void setUp(){
		Azienda azienda = new Azienda();
		azienda.setRagioneSociale("anoki");
		aziendaDAO.createAzienda(azienda);
		
		Telefono telefono = new Telefono();
		telefono.setNumero("1111");
		telefonoDAO.createTelefono(telefono);
		
	}
	
	@Test
	@Transactional
	public void setStato(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
		
		aziendaDAO.clear();
		
		Assert.assertTrue(recapitoAziendaDAO.findById(recapitoAzienda.getId()).getStato());
	}
	
	@Test
	@Transactional
	public void up(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
		
		aziendaDAO.clear();
		recapitoAziendaService.up(recapitoAzienda);
		
		Assert.assertEquals(1, recapitoAziendaDAO.findById(recapitoAzienda.getId()).getVoti());
	}
	
	@Test
	@Transactional
	public void down(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
		
		aziendaDAO.clear();
		recapitoAziendaService.down(recapitoAzienda);
		
		Assert.assertEquals(-1, recapitoAziendaDAO.findById(recapitoAzienda.getId()).getVoti());
	}
	
	@Test
	@Transactional
	public void updown(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
		
		aziendaDAO.clear();
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.up(recapitoAzienda);
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.up(recapitoAzienda);
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.up(recapitoAzienda);

		
		Assert.assertEquals(0, recapitoAziendaDAO.findById(recapitoAzienda.getId()).getVoti());
	}
	
	
	@Test
	@Transactional
	public void updownStato(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
		
		aziendaDAO.clear();
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.up(recapitoAzienda);
		recapitoAziendaService.up(recapitoAzienda);
		recapitoAziendaService.up(recapitoAzienda);

		
		Assert.assertTrue(recapitoAziendaDAO.findById(recapitoAzienda.getId()).getStato());
	}
	
	
	
	@Test
	@Transactional
	public void downStato(){
		RecapitoAzienda recapitoAzienda = new RecapitoAzienda();
		recapitoAzienda.setAzienda(aziendaDAO.findByRagioneSociale("anoki"));
		recapitoAzienda.setTelefonoAzienda(telefonoDAO.findByNumero("1111"));
		recapitoAzienda.setDescrizione("amministrazione");
		recapitoAziendaDAO.createRecapitoAzienda(recapitoAzienda);
		
		aziendaDAO.clear();
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.down(recapitoAzienda);
		recapitoAziendaService.down(recapitoAzienda);

		Assert.assertFalse(recapitoAziendaDAO.findById(recapitoAzienda.getId()).getStato());
	}
	
}
