insert into azienda(azienda_id, ragione_sociale, indirizzo, codice_postale, citta, stato) 
	values(1, 'anoki', 'via g.g.mora', '20090', 'milano','italia');

insert into azienda(azienda_id, ragione_sociale, indirizzo, codice_postale, citta, stato) 
	values(2, 'nike', 'via lavoratori', '21456', 'Roma','italia');
	
insert into persona(persona_id, cognome, nome, azienda_id, profilo, email)
	values(1, 'pippo', 'pluto', 1, 'amministratore', 'pippo.pluto@mail.com');
	
insert into persona(persona_id, cognome, nome, azienda_id, profilo, email)
	values(2, 'marco', 'rossi', 2, 'receptionist', 'marco.rossi@mail.com');
	
insert into persona(persona_id, cognome, nome, azienda_id, profilo, email)
	values(3, 'luca', 'bianchi', 1, 'senior', 'luca.bianchi@mail.com');